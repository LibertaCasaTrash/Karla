# Karla - IRC Hybrid Open Proxy Monitor

This is a `hopm` instance to detect and manage potentially abusive hosts via dnsbls and port scans



## Installation - Productionized

> Requires root priviledges

1. Add an unpriviledged role user `Karla` with the home `/opt/hopm/`.
2. Add yourself to the group `Karla`.
3. `cd /opt/hopm/` and `git clone <repo>`.
4. `cd Karla/` and `cp Karla.service /etc/systemd/system/`.
5. Check `/opt/hopm/Karla/etc/hopm.conf` and configure accordingly.
5. `systemctl daemon-reload` and `systemctl enable --now Karla.service`.


## Credentials

* Ensure `hopm.conf` and `Karla.conf` is in `.gitignore` 


## TODO

* Deploy Utility which can pull the systemd unit file from `LibertaCasa/system` repo
* Lint checking before deploy 
